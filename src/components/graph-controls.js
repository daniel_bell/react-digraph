import React, { Component } from 'react';
import Radium from 'radium';
import PropTypes from 'prop-types';

const FaExpand = require('react-icons/lib/fa/expand');

const steps = 100; // Slider steps

function makeStyles(primary) {
    return {
        controls: {
            position: 'absolute',
            bottom: '25%',
            left: '75%',
            zIndex: 100,
            display: 'grid',
            gridTemplateColumns: 'auto auto',
            gridGap: '15px',
            alignItems: 'center'
        },
        sliderWrapper: {
            backgroundColor: '#FFFFd0',
            color: primary,
            border: `solid 1px black`,
            padding: '6.5px',
            borderRadius: '5px'
        },
        slider: {
            position: 'relative',
            top: '6px',
            marginLeft: 5,
            marginRight: 5,
            cursor: 'pointer'
        },
        button: {
            backgroundColor: '#FFFFd0',
            color: primary,
            border: `solid 1px black`,
            outline: 'none',
            width: 35,
            height: 35,
            borderRadius: '5px',
            cursor: 'pointer'
        }
    };
}

class GraphControls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            styles: makeStyles(props.primary)
        };
    }

    // Convert slider val (0-steps) to original zoom value range
    sliderToZoom(val) {
        return (
            val * (this.props.maxZoom - this.props.minZoom) / steps +
            this.props.minZoom
        );
    }

    // Convert zoom val (minZoom-maxZoom) to slider range
    zoomToSlider(val) {
        return (
            (val - this.props.minZoom) *
            steps /
            (this.props.maxZoom - this.props.minZoom)
        );
    }

    // Center graph-view on contents of svg > view
    zoomToFit() {
        this.props.zoomToFit();
    }

    // Modify current zoom of graph-view
    zoom(e) {
        const sliderVal = e.target.value;
        const zoomLevelNext = this.sliderToZoom(sliderVal);
        const delta = zoomLevelNext - this.props.zoomLevel;

        if (
            zoomLevelNext <= this.props.maxZoom &&
            zoomLevelNext >= this.props.minZoom
        ) {
            this.props.modifyZoom(delta);
        }
    }

    render() {
        const styles = this.state.styles;

        return (
            <div style={styles.controls} className="graphControls">
                <div style={styles.sliderWrapper}>
                    -
                    <input
                        type="range"
                        style={styles.slider}
                        min={this.zoomToSlider(this.props.minZoom)}
                        max={this.zoomToSlider(this.props.maxZoom)}
                        value={this.zoomToSlider(this.props.zoomLevel)}
                        onChange={this.zoom.bind(this)}
                        step="1"
                    />
                    +
                </div>
                <button style={styles.button} onMouseDown={this.props.zoomToFit}>
                    <FaExpand />
                </button>
            </div>
        );
    }
}

GraphControls.propTypes = {
    primary: PropTypes.string,
    minZoom: PropTypes.number,
    maxZoom: PropTypes.number,
    zoomLevel: PropTypes.number.isRequired,
    zoomToFit: PropTypes.func.isRequired,
    modifyZoom: PropTypes.func.isRequired
};

GraphControls.defaultProps = {
    primary: 'dodgerblue',
    minZoom: 0.15,
    maxZoom: 1.5
};

export default Radium(GraphControls);
